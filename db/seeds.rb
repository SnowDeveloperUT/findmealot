# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

more_users = [{username: 'user1'}, {username: 'user2'}]
more_parkinglots = [{streetaddress: 'Vanemuise 15'},
                    {streetaddress: 'Turu 2'},
                    {streetaddress: 'Raatuse 21'},]

more_parkinglotdetails = [{priceperhour: 0.80, freelots: 15, distance: 1.2},
                          {priceperhour: 1.00, freelots: 30, distance: 2.0},
                          {priceperhour: 0.50, freelots: 20, distance: 2.5}]

more_parkinglotdetails.each do |parkinglotdetail|
  new_parkinglotdetail = Parkinglotdetail.new({priceperhour: parkinglotdetail[:priceperhour],
                                               freelots: parkinglotdetail[:freelots],
                                               distance: parkinglotdetail[:distance]})
  new_parkinglotdetail.user = more_users.last
  new_parkinglotdetail.parkinglot = Parkinglot.create(more_parkinglots.pop)
  new_parkinglotdetail.save!
end