class CreateParkinglotdetails < ActiveRecord::Migration[5.0]
  def change
    create_table :parkinglotdetails do |t|
      t.float 'priceperhour'
      t.integer 'freelots'
      t.float 'distance'
    end
  end
end
