class Parkinglotdetail < ApplicationRecord
  belongs_to :parkinglot, optional: true
  belongs_to :user, optional: true
end
