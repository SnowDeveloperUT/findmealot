class Parkinglot < ApplicationRecord
  validates :streetaddress, presence: true
  has_many :parkinglotdetails
  has_many :users, :through => :parkinglotdetails
end
