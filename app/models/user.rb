class User < ApplicationRecord
  validates :username, presence: true, uniqueness: true
  has_many :parkinglots, :through => :parkinglotdetails
  has_many :parkinglotdetails
end
