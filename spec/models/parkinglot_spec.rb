require 'rails_helper'

RSpec.describe Parkinglot, type: :model do

  context 'Relationship' do

    before do
      @parkinglot = FactoryGirl.create(:@parkinglot)
    end

    it 'should not save without address' do
      @parkinglot = Parkinglot.new
      expect(@parkinglot.save).to be_falsy
    end

    it 'has a valid factory' do
      expect(build(:parkinglot)).to be_valid
    end


  end


end
