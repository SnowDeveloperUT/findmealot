require 'spec_helper'

describe User do

  it 'should not submit without address' do
    expect{FactoryGirl.create(:user, streetaddress: nil)}.to raise_error(ActiveRecord::RecordInvalid)
  end

  it 'has a valid factory' do
    expect(build(:streetaddress)).to be_valid
  end



end
