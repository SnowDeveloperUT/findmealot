FactoryGirl.define do
  factory :parkinglotdetail do
    priceperhour Faker::Number.decimal(2)
    freelots Faker::Number.digit
    distance Faker::Number.decimal(2)
  end
end
