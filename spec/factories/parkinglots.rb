FactoryGirl.define do
  factory :parkinglot do
    streetaddress Faker::Parkinglot.name
  end
end
