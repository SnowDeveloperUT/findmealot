Feature: Free Parking Lots Query

  Background: parkinglots have been added to database

    Given the following parkinglots exist:
      | streetaddress           | priceperhour | freelots      | distance         | username |
      | Vanamuise 15            | 0.80         | 15            | 1.2               | testuser |
      | Turu 2                  | 1.00         | 30            | 2.0              | testuser |
      | Raatuse 21              | 0.50         | 20            | 2.5              | testuser |

  Scenario: can view the parkinglots page
    Given I am on the home page
    Then I should see "Query parking lots"

  Scenario: can view the parkinglots page
    Given I am on the home page
    Then I should see "Reference address"

  Scenario: can view the list of free parking lots
    When I go to the home page
    And  I fill in "Reference address" with "Liivi 2"
    When I press "Submit"
    Then I should see "Vanemuise 15"
    And I should see "Turu 2"
    And I should see "Raatuse 21"